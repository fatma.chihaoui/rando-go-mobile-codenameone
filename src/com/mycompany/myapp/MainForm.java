/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;

/**
 *
 * @author Me
 */
public class MainForm {
    static Form f;
    
    public MainForm(Resources theme){
            UIBuilder ui = new UIBuilder();
    
  
   
        f = new Form("", BoxLayout.y());
         f.setUIID("Formm");
              
            Image logo = theme.getImage("fond.jpg"); 
            System.out.println(logo);
            f.setBgImage(logo);
            f.getToolbar().addCommandToRightBar("Inscrir", null, (ActionListener) (ActionEvent evt) -> {
                InsertForm user = new InsertForm(theme);
                user.getF().show();
            });
            f.getToolbar().addCommandToLeftBar("Login", null, (ActionListener) (ActionEvent evt) -> {
                AuthForm auth = new AuthForm(theme);
                auth.f.show();           
            });
            f.show(); 
    }
    
    public void showForm(){
        f.show();
    }
    
}
