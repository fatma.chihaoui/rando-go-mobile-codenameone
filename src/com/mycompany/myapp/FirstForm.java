/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.MediaPlayer;
import com.codename1.io.Log;
import com.codename1.io.Preferences;
import com.codename1.media.Media;
import com.codename1.media.MediaManager;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import static com.mycompany.myapp.MyApplication.theme;
import java.io.IOException;

/**
 *
 * @author Me
 */
public class FirstForm {
    
    Form form;
    
    
    public FirstForm(){
       
        System.out.println(Preferences.get("id",null));
        
        
       form = new Form("welcome", new BorderLayout());
        Container c=new Container(new BorderLayout());
        form.add(BorderLayout.CENTER,c);
        c.getStyle().setPaddingTop(70);
        initToolBar.init(form.getToolbar());


 try {
                    form.refreshTheme();
                    Media video = MediaManager.createMedia(theme.getData("comp1.mp4"), "video/mp4");
                    c.removeAll();
                    video.play();
                    form.refreshTheme();
                    c.add(BorderLayout.CENTER, new MediaPlayer(video));
                    c.revalidate();
                    form.refreshTheme();
                } catch(IOException err) {
                    Log.e(err);
                }
      
    }
    
    public void showForm(){
        form.show();
        form.refreshTheme();
    }
    
}
