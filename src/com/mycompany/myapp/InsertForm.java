/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Preferences;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;

/**
 *
 * @author user
 */
public class InsertForm {

    public TextField getUsername() {
        return username;
    }

    public void setUsername(TextField username) {
        this.username = username;
    }

    public TextField getEmail() {
        return email;
    }

    public void setEmail(TextField email) {
        this.email = email;
    }
    
    

    public Form getF() {
        return f;
    }

    public TextField getNom() {
        return nom;
    }


    public TextField getPassword() {
        return password;
    }

    public TextField getMail() {
        return email;
    }

    public Button getBtnOk() {
        return btnOk;
    }
    
    
    
    
    
    
private Form f = new Form("Insert", BoxLayout.y());



TextField password = new TextField("", "password" ,20, TextArea.PASSWORD  );
TextField username = new TextField("", "username",20, TextArea.ANY);
TextField nom = new TextField("", "nom",20, TextArea.ANY);
TextField email = new TextField("", "email",20,TextArea.ANY);

private Button btnOk = new Button("Ajouter");
    
    public InsertForm(Resources theme)
    
    {
     
       
        f= new Form();
        Image bg = theme.getImage("3.jpg");
        Image ad = theme.getImage("ad.png");
        Image back = theme.getImage("Backn.png");   
        f.getToolbar().addCommandToLeftBar("Back", back, (ActionListener) (ActionEvent evt) -> {
            MainForm.f.showBack();
        });
            
        f.setBgImage(bg);
        btnOk.setIcon(ad);
        f.add(nom);
        f.add(password);
        f.add(email);
        f.add(username);
        f.add(btnOk) ;
        btnOk.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                ConnectionRequest req = new ConnectionRequest();
                req.setUrl("http://localhost/PIDdevMobile/insert/insert.php?"
                        +"nom=" + nom.getText()
                        +"&password=" + password.getText()
                        + "&email=" + email.getText()
                        +"&username=" + username.getText());

                req.addResponseListener((NetworkEvent evt1) -> {
                    byte[] data = (byte[]) evt1.getMetaData();
                    String s = new String(data);
                    System.out.println(s);
                    if (s.equals("success")) {
                        MainForm.f.showBack();
                    } else {
                        Dialog.show("Erreur", "erreur", "Ok", null);
                    }
                });

                NetworkManager.getInstance().addToQueue(req);
            }
        });
    
}

                

   
            
    
    
    
    
    
}
