/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import entity.Reclamation;
import com.codename1.components.ImageViewer;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Preferences;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author user
 */
public class ListReclamationsForm {
    
    
    Resources theme;

    public Form getF() {
        return f;
    }
    
    
  
    
    private Form f;

  public ListReclamationsForm(Resources theme)
  {
      f = new Form("mes reclamations",BoxLayout.y());
      
      
      this.theme=theme;
      ArrayList<Reclamation> listrec = new ArrayList<>();
      
      ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIDdevMobile/pi/select.php?loginUser="+Preferences.get("id",null));
        con.addResponseListener((NetworkEvent evt) -> {
            System.out.println(GetListReclamations(new String(con.getResponseData())));
            listrec.addAll(GetListReclamations(new String(con.getResponseData())));
            int i=0;
            if (i<= listrec.size())
                for (Reclamation r : listrec) {
                    
                    addItem(listrec.get(i));
                    i++;
                }
            f.refreshTheme();
      });
      NetworkManager.getInstance().addToQueue(con);
   
     

    // f.show();
      
      
      
      
  }
         
         
         
         
       public void addItem(Reclamation reclamation) {

        Container C1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
         
        Image rec = theme.getImage("reclamation.png");
        
        ImageViewer img = new ImageViewer();
        img.setImage(rec);

        Container C2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label l =new Label();
        l.setText(reclamation.getObjet()+"");
         
        l.addPointerPressedListener((ActionListener) (ActionEvent evt) -> {
            //  Dialog.show("test", l.getText() ,"Ok",null);
            Container C3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Label titreReclamation = new Label();
            Label message = new Label();
            message.setText(reclamation.getMessage());
            titreReclamation.setText(reclamation.getObjet());
            
            C3.add(titreReclamation);
            C3.add(message);
            Button supprimer = new Button("supprimer");
            Button modifier = new Button("modifier");
            Form details = new Form(BoxLayout.y());
            
            
            C3.add(supprimer);
            C3.add(modifier);
            details.addComponent(C3);
            details.show();
            
            
            supprimer.addActionListener((ActionListener) (ActionEvent evt1) -> {
                ConnectionRequest con = new ConnectionRequest();
                con.setUrl("http://localhost/PIDdevMobile/pi/delete.php?id="+ reclamation.getId());
                con.addResponseListener(new ActionListener<NetworkEvent>() {
                    @Override
                    public void actionPerformed(NetworkEvent evt2) {
                        byte[] data = (byte[]) evt2.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                        if (s.equals("success")) {
                            Dialog.show("Confirmation", "reclamation supprimée", "Ok", null);
                        } else {
                            Dialog.show("Erreur", "erreur", "Ok", null);
                        }
                         (new ListReclamationsForm(theme)).getF().show();
                    }
                });
                NetworkManager.getInstance().addToQueue(con);
            });
            
            modifier.addActionListener((ActionListener) (ActionEvent evt1) -> {
                
                ReclamationForm modif = new ReclamationForm(theme);
                Button valider = new Button("valider");
                // modif.setBtnOk("valider");
                modif.getF().add(valider);
                modif.getBtnOk().setVisible(false);
                modif.getObjet().setText(reclamation.getObjet()+"");
                modif.getMessage().setText(reclamation.getMessage()+"");
                modif.getF().show();
                valider.addActionListener((ActionListener) (ActionEvent evt2) -> {
                    ConnectionRequest req = new ConnectionRequest();
                    req.setUrl("http://localhost/PIDdevMobile/pi/update.php?id="+ reclamation.getId()+ "&objet=" + modif.getObjet().getText()+ "&message=" + modif.getMessage().getText() + "");
                    req.addResponseListener((NetworkEvent evt3) -> {
                        byte[] data = (byte[]) evt3.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                        if (s.equals("success")) {
                            Dialog.show("Confirmation", "modif ok", "Ok", null);
                        } else {
                            Dialog.show("Erreur", "erreur", "Ok", null);
                        }
                           (new ListReclamationsForm(theme)).getF().show();
                    });
                    NetworkManager.getInstance().addToQueue(req);
                });
            });
        }); 
        C2.add(l);
      
        C1.addComponent(img);
        C1.addComponent(C2);
        C1.setLeadComponent(l);
        f.addComponent(C1);
       
        //f.refreshTheme();
        
   
    
    
}
      
 
       
       
       public ArrayList<Reclamation> GetListReclamations(String json)
      { 
          System.out.println(json);
           ArrayList<Reclamation> listReclamations = new ArrayList<>();

        try {

            JSONParser j = new JSONParser();

            Map<String, Object> reclamations = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) reclamations.get("reclamation");

            for (Map<String, Object> obj : list) {
                Reclamation r = new Reclamation();
             
              r.setId(Integer.parseInt(obj.get("id").toString()));
                r.setObjet(obj.get("objet").toString());
                r.setMessage(obj.get("message").toString());
                listReclamations.add(r);

            }

        } catch (IOException ex) {
         }
        return listReclamations;
      
      }
  
  
  }
  







