/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Preferences;
import com.codename1.ui.Button;

import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;



/**
 *
 * @author user
 */
public class ReclamationForm {

     private Form f = new Form("Insert", BoxLayout.y());

        
        TextField Objet ;
        TextArea Message ;
        Label btnOk;
        
    public ReclamationForm(Resources theme)
    
    {
        f= new Form();
        
        
        Objet = new TextField();
        Message = new TextArea();
        btnOk = new Label();

        Message.setPreferredSize(new Dimension(280, 100));
        
        f.add(Objet);
        f.add(Message);

        Image ajouter = theme.getImage("add.png");
        btnOk.setIcon(ajouter);
        f.add(btnOk) ;
        

        
        btnOk.addPointerReleasedListener((ActionListener) (ActionEvent evt) -> {
           
            ConnectionRequest req = new ConnectionRequest();
            req.setUrl("http://localhost/PIDdevMobile/pi/insert.php?Objet=" + Objet.getText() + "&Message=" + Message.getText().toString()+"&id_user="+Preferences.get("id",null));
            
            req.addResponseListener((NetworkEvent evt1) -> {
                byte[] data = (byte[]) evt1.getMetaData();
                String s = new String(data);
                System.out.println(s);
                if (s.equals("success")) {
                    Dialog.show("Confirmation", "ajout ok", "Ok", null);
                } else {
                    Dialog.show("Erreur", "erreur", "Ok", null);
                }
                ListReclamationsForm liste = new ListReclamationsForm(theme);
                liste.getF().show();
            });
            NetworkManager.getInstance().addToQueue(req);

        });
        
        
     
        
        
      
}

    public Label getBtnOk() {
        return btnOk;
    }

    public void setBtnOk(Label btnOk) {
        this.btnOk = btnOk;
    }
                

    /**
     * @return the f
     */
    public Form getF() {
        return f;
    }

    /**
     * @param f the f to set
     */
    public void setF(Form f) {
        this.f = f;
    }

    /**
     * @return the Objet
     */
    public TextField getObjet() {
        return Objet;
    }

    /**
     * @param Objet the Objet to set
     */
    public void setObjet(TextField Objet) {
        this.Objet = Objet;
    }

    /**
     * @return the Message
     */
    public TextArea getMessage() {
        return Message;
    }

    /**
     * @param Message the Message to set
     */
    public void setMessage(TextArea Message) {
        this.Message = Message;
    }

    
    
    
    
    
}
