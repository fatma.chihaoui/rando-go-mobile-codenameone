/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.io.Preferences;
import com.codename1.ui.Form;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import entity.Randonnee;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Me
 */
public class ListRandoForm {
    
    Form form;
    
    String url="http://localhost/PIDdevMobile/list_rando.php?connected_user_id="+Preferences.get("id",null);
    ConnectionRequest connection;
    StringBuffer response;
    
    
    public ListRandoForm(Resources theme){
        
        form=new Form("list randonnees",new BoxLayout(BoxLayout.Y_AXIS));
        initToolBar.init(form.getToolbar());
        
        response=new StringBuffer();
        connection=new ConnectionRequest(){
            
          
            @Override
            protected void readResponse(InputStream input) throws IOException {
                int ch;
                while((ch=input.read())!=-1){
                    response.append((char)ch);
                   
                }
            }

            @Override
            protected void postResponse() {
                String responseString="{\"root\":"+(response.toString())+"}";
                //String s=responseString.replaceAll("","");
              
                //System.out.println(responseString);
                System.out.println(Services.getListRando(responseString));
                for(Randonnee rando : Services.getListRando(responseString)){
                    form.add(rando.getContainer());
                }
                
            }  
        };
        
        connection.setUrl(url);
        NetworkManager.getInstance().addToQueueAndWait(connection);
        
        
        
       
        
        
   
        
    }
    
    public void showForm(){
        this.form.show();
    }
    
}
