/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Display;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import static com.mycompany.myapp.MyApplication.theme;

/**
 *
 * @author Me
 */
public class initToolBar {
    
    public static void init(Toolbar toolbar){
        
        toolbar.addCommandToOverflowMenu("Logout", null, (ActionListener) (ActionEvent evt) -> {
            //Display.exitApplication();
        });
        
        
        
        
        toolbar.addCommandToSideMenu("list Rando", null, (ActionListener) (ActionEvent evt) -> {
            (new ListRandoForm(theme)).showForm();
           
        });
        toolbar.addCommandToSideMenu("mes participation", null, (ActionListener) (ActionEvent evt) -> {
            (new ListParticipationForm(theme)).showForm();
        });
        
        toolbar.addCommandToSideMenu("recherche", null, (ActionListener) (ActionEvent evt) -> {
            (new RechercheForm()).showForm();
        });
        
         toolbar.addCommandToSideMenu("Mes Reclamations", null, (ActionListener) (ActionEvent evt) -> {
           (new ListReclamationsForm(theme)).getF().show(); 
        });
         
          toolbar.addCommandToSideMenu("Nouvelle réclamation", null, (ActionListener) (ActionEvent evt) -> {
           ( new ReclamationForm(theme)).getF().show();
              
        });
        
        
        
    }
    
}
