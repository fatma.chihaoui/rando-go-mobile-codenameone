/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import entity.Participation;
import entity.Randonnee;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author Me
 */
public class DetRandoForm {
    Form form;
    int imageCounter=0;
    StringBuffer response;
    Participation p;
    Resources theme;
    
   public DetRandoForm(Resources theme,Randonnee rando){
       
       this.theme=theme;
       
       if(rando.getIsParticipating()!=0){
           p=new Participation();
           p.setDate(rando.getDate_part());
           p.setId(rando.getIsParticipating());
       }
       
       
       UIBuilder.registerCustomComponent("ImageViewer",ImageViewer.class);
       UIBuilder.registerCustomComponent("SpanLabel", SpanLabel.class);
        
       UIBuilder ui=new UIBuilder();
        form=(Form) ui.createContainer(theme,"detRando");
        initToolBar.init(form.getToolbar());
        
        if(rando.getIsParticipating()==0){
            ((Button) ui.findByName("annpartButton", form)).setVisible(false);
            ((Button) ui.findByName("pdfButton", form)).setVisible(false);
            ((Button) ui.findByName("rateButton", form)).setVisible(false);
            
        }else{
            ((Button) ui.findByName("participerButton", form)).setVisible(false);
            
            
        }
        
        
        
        //participer button
        ((Button) ui.findByName("participerButton", form)).addActionListener(al->{
        String url="http://localhost/PIDdevMobile/participer.php?id_user=3&id_rando="+rando.getId();
        ConnectionRequest connection;
        response=new StringBuffer();
        connection=new ConnectionRequest(){
            @Override
            protected void readResponse(InputStream input) throws IOException {
                int ch;
                while((ch=input.read())!=-1){
                    response.append((char)ch); 
                }
            }
            @Override
            protected void postResponse() {
                String responseString=(response.toString());
                p=Services.getParticipation("{\"root\":"+(response.toString())+"}");
                System.out.println(responseString);
                ((Button) ui.findByName("annpartButton", form)).setVisible(true);
                ((Button) ui.findByName("participerButton", form)).setVisible(false);
                ((Button) ui.findByName("pdfButton", form)).setVisible(true);
                ((Button) ui.findByName("rateButton", form)).setVisible(true);
                form.refreshTheme();
            }  
        };
        connection.setUrl(url);
        NetworkManager.getInstance().addToQueueAndWait(connection);
        
        });
        
        
        //annuler participation button
        ((Button) ui.findByName("annpartButton", form)).addActionListener(al->{
        String url="http://localhost/PIDdevMobile/ann_participation.php?id_user=3&id_rando="+rando.getId();
        ConnectionRequest connection;
        response=new StringBuffer();
        connection=new ConnectionRequest(){
            @Override
            protected void readResponse(InputStream input) throws IOException {
                int ch;
                while((ch=input.read())!=-1){
                    response.append((char)ch); 
                }
            }
            @Override
            protected void postResponse() {
                String responseString=(response.toString());
                System.out.println(responseString);
                ((Button) ui.findByName("annpartButton", form)).setVisible(false);
                ((Button) ui.findByName("participerButton", form)).setVisible(true);
                ((Button) ui.findByName("pdfButton", form)).setVisible(false);
                ((Button) ui.findByName("rateButton", form)).setVisible(false);
                form.refreshTheme();
            }  
        };
        connection.setUrl(url);
        NetworkManager.getInstance().addToQueueAndWait(connection);
        });
        
        //evaluer button
        ((Button) ui.findByName("rateButton", form)).addActionListener(al->{
           
            (new Rating(theme, rando)).getF().show();
            
        });
        
        
        
        //generate pdf button
        ((Button) ui.findByName("pdfButton", form)).addActionListener(al->{
            (new PDFHandler()).getFile(new String[]{
                rando.getId()+"",
                rando.getTitre(),
                rando.getPrix()+"",
                rando.getDate().toString(),//date rando
                p.getId()+"",
                "3",
                p.getDate().toString()
            },
                    "participation_"+rando.getIsParticipating()+(new Random(100)).nextInt()+".pdf");
        });
        
        
        
        
        
        //init data from instance
        Calendar cal=Calendar.getInstance();
        cal.setTime(rando.getDate());
        
        Label l=(Label) ui.findByName("Label", form);
        l.setText(rando.getTitre());
        
        EncodedImage encodedImage=EncodedImage.createFromImage(MyApplication.theme.getImage("Loading.png"),false);
        Image i=URLImage.createToStorage(encodedImage,rando.getListImages().get(0).getId()+"","http://localhost:8000/Uploads/"+rando.getListImages().get(0).getPath(),URLImage.RESIZE_SCALE_TO_FILL);
        i=i.scaled(250, 300);
        ((ImageViewer)ui.findByName("ImageViewer", form)).setImage(i);
       
       Label prix_date=(Label) ui.findByName("prix_date", form);
       prix_date.setText(rando.getPrix()+"dt "+cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR));
       
       Label typeRando=(Label) ui.findByName("typeRando", form);
       typeRando.setText(rando.getType());
       
       SpanLabel description=(SpanLabel) ui.findByName("description", form);
       description.setText(rando.getDescription()); 
   }
   
   public void showForm(){
       this.form.show();
   }
    
}
