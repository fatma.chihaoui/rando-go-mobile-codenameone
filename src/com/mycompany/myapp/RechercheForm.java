/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.util.UIBuilder;
import entity.Randonnee;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Me
 */
public class RechercheForm {
    Form form;

    String url="http://localhost/PIDdevMobile/rechercher.php?data=";
    ConnectionRequest connection;
    StringBuffer response;
    Container c;
    
    
    public RechercheForm() {
    
        UIBuilder ui=new UIBuilder();
        form=(Form) ui.createContainer(MyApplication.theme,"Rechercher");
        c=new Container();
        form.add(c);
        initToolBar.init(form.getToolbar());
        response=new StringBuffer();
        
       ((Button) ui.findByName("recherche_button", form)).addActionListener(l->{
           
            connection=new ConnectionRequest(){
            @Override
            protected void readResponse(InputStream input) throws IOException {
                int ch;
                while((ch=input.read())!=-1){
                    response.append((char)ch);
                   
                }
            }

            @Override
            protected void postResponse() {
                String responseString="{\"root\":"+(response.toString())+"}";
                //String s=responseString.replaceAll("","");
              
                //System.out.println(responseString);
                System.out.println(Services.getListRando(responseString));
                c.removeAll();
                for(Randonnee rando : Services.getListRando(responseString)){
                    c.add(rando.getContainer());
                }
                form.refreshTheme();
                
            }  
        };
            
             connection.setUrl(url+((TextField)ui.findByName("recherche_textfield", form)).getText());
        NetworkManager.getInstance().addToQueueAndWait(connection);
           
       });
       
       
       
        
        
       
        
       
        
        
        
    }
    
    
    
    public void showForm(){
        form.show();
    }
    
}
