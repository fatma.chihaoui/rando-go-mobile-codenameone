/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Form;
import com.codename1.ui.util.Resources;
import entity.Randonnee;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Me
 */
public class ListParticipationForm {
    
    Form form;
    
    String url="http://localhost/PIDdevMobile/list_participation.php?id_user=3";
    ConnectionRequest connection;
    StringBuffer response;
    
    public ListParticipationForm(Resources theme){
        
        form=new Form("mes participations");
        initToolBar.init(form.getToolbar());
        
        
        response=new StringBuffer();
        connection=new ConnectionRequest(){
            @Override
            protected void readResponse(InputStream input) throws IOException {
                int ch;
                while((ch=input.read())!=-1){
                    response.append((char)ch);
                   
                }
            }

            @Override
            protected void postResponse() {
                String responseString="{\"root\":"+(response.toString())+"}";
                //String s=responseString.replaceAll("","");
              
                //System.out.println(responseString);
                System.out.println(Services.getListRando(responseString));
                for(Randonnee rando : Services.getListRando(responseString)){
                    form.add(rando.getContainer());
                }
                
            }  
        };
        
        connection.setUrl(url);
        NetworkManager.getInstance().addToQueueAndWait(connection);
        
        
        
        
    }
    
    public Form getForm(){
        return form;
    }
    
    public void showForm(){
        form.show();
    }
    
}
