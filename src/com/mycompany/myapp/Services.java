/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;


import entity.Randonnee;
import com.codename1.io.CharArrayReader;
import com.codename1.io.JSONParser;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import entity.Participation;
import entity.RandoImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;



/**
 *
 * @author Me
 */
public class Services {
    
    
    public static Participation getParticipation(String json){
        Participation e=new Participation();
        try{
            JSONParser j = new JSONParser();
            Map<String, Object> participation = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) participation.get("root");
                
            Map<String, Object> obj =list.get(0);
           
                e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setDate(new Date());
                
            
        } catch (IOException ex) {
         }
        
        return e;
    }
    
    public static ArrayList<Randonnee> getListRando(String json){
        ArrayList<Randonnee> listRando = new ArrayList<>();
      try {
            JSONParser j = new JSONParser();
            Map<String, Object> etudiants = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) etudiants.get("root");

            for (Map<String, Object> obj : list) {
                Randonnee e = new Randonnee();
                e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setTitre(obj.get("titre").toString());
                e.setDescription(obj.get("description").toString());
                e.setType(obj.get("type").toString());
                e.setDestination(obj.get("destination").toString());
                e.setPrix(Integer.parseInt(obj.get("prix").toString()));
                e.setNb_place_max(Integer.parseInt(obj.get("nb_place_max").toString()));
                e.setNb_place_min(Integer.parseInt(obj.get("nb_place_min").toString()));
                e.setNb_kilometres(Integer.parseInt(obj.get("nb_kilometres").toString()));
                        if(obj.get("id_participation")==null){
                    e.setIsParticipating(0);
                }else
                {
                     e.setIsParticipating(Integer.parseInt(obj.get("id_participation").toString()));
                     
                      Date date;
                    try {
                        date = new SimpleDateFormat("yyyy-MM-dd").parse(obj.get("date_part").toString());
                        e.setDate_part(date);
                    } catch (ParseException ex) {
                        
                    }
                    
                }
                
                try {
                    Date date = new SimpleDateFormat("yyyy-MM-dd").parse(obj.get("date_rando").toString());
                    e.setDate(date);
                    
                } catch (ParseException ex) {
                   
                }
                
                //e.setOrganisateur(obj.get("organisateur").toString())
                List<Map<String, Object>> sublist = (List<Map<String, Object>>) obj.get("list_images");
                e.setListImages(getListImages(sublist));
                listRando.add(e);
            }
        } catch (IOException ex) {
         }
        return listRando;
    }
    
    private static ArrayList<RandoImage> getListImages(List<Map<String, Object>> sublist){
         ArrayList<RandoImage> listImages = new ArrayList<>();
        for (Map<String, Object> obj : sublist) {
         listImages.add(new RandoImage(Integer.parseInt(obj.get("id").toString()),obj.get("path").toString()));
         }
        return listImages;
    }
    
}
