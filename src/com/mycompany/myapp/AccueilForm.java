/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.ListReclamationsForm;
import com.mycompany.myapp.Rating;
import com.mycompany.myapp.ReclamationForm;
import entity.Randonnee;


/**
 *
 * @author user
 */
public class AccueilForm {
    
    private Form f = new Form(BoxLayout.y());
    
   

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

  
    
    
    
    public AccueilForm(Resources theme)
    {
       
        
      //  Container c = new Container();
     
    
        Image rec = theme.getImage("reclamation.png");
        Image logo = theme.getImage("randologo.png");
        Image rate = theme.getImage("star1.png");
        Image home = theme.getImage("home.png");
        f.setBgImage(logo);
        //icone.setIcon(rec);
       // f.add(c);
       
       
         f.getToolbar().addCommandToSideMenu("Nouvelle réclamation", rec, (ActionListener) (ActionEvent evt) -> {

             ReclamationForm ajout = new ReclamationForm(theme);
             
             ajout.getF().getToolbar().addCommandToLeftBar("", theme.getImage("back-command.png"), (ActionListener) (ActionEvent evt1) -> {
                 f.showBack();
             });
             
             ajout.getF().show();
   
        });
        f.getToolbar().addCommandToSideMenu("Mes Reclamations", rec, (ActionListener) new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Resources reclamations = UIManager.initFirstTheme("/theme");
                ListReclamationsForm listerec = new ListReclamationsForm(reclamations);
                listerec.getF().show();
                
                listerec.getF().getToolbar().addCommandToLeftBar("", theme.getImage("back-command.png"), (ActionListener) (ActionEvent evt1) -> {
                    f.showBack();
                });
            }
        });
 
       f.getToolbar().addCommandToSideMenu("Evaluer mes randonnées", rate, (ActionListener) (ActionEvent evt) -> {
           Resources reclamations = UIManager.initFirstTheme("/theme");
           Randonnee ra=new Randonnee();
           ra.setId(1);
           Rating r = new Rating(reclamations,ra);
           r.getF().show();
           
           r.getF().getToolbar().addCommandToLeftBar("", theme.getImage("back-command.png"), (ActionListener) (ActionEvent evt1) -> {
               f.showBack();
           });
        });
    
    }
    
    
}
