/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.FileSystemStorage;
import com.codename1.io.Util;
import com.codename1.ui.Button;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.layouts.BoxLayout;

public class PDFHandler {

private final static String URL="http://api.html2pdfrocket.com/pdf";
private final static String APIKEY = "63f1824f-4009-42c4-9887-c9b389628c93";


/**
 * Stores given HTML String or URL to Storage with given filename
 * @param value URL or HTML add quote if you have spaces. use single quotes instead of double
 * @param filename
 */
public void getFile(String[] values,String filename){
    // Validate parameters
    
    String value=
"<html>\n" +
"<style>\n" +
"td {\n" +
"	padding: 10px;\n" +
"    \n" +
"}\n" +
"</style>\n" +
"<div style=\"float:center\">\n" +
"<center>\n" +
"<img src=\"http://i.imgur.com/X4PqY3b.png\" width=\"100px\" height=\"100px\">\n" +
"</center>\n" +
"</div>\n" +
"<br>\n" +
"<div style=\"clear: both;\">\n" +
"<center>\n" +
"<fieldset style=\"width: 70%;\">\n" +
"    <legend>Info:</legend>\n" +
"    <center>\n" +
"  <table >\n" +
"<tr>\n" +
"<td>id randonnee\n" +
"<td>titre\n" +
"<td>prix\n" +
"<td>date randonnee\n" +
"<td>id participation\n" +
"<td>id participant\n" +
"<td>date participation\n" +
"\n" +
"</tr>\n" +
"<tr>\n" +
"<td>"+values[0]+"\n" +
"<td>"+values[1]+"\n" +
"<td>"+values[2]+"\n" +
"<td>"+values[3]+"\n" +
"<td>"+values[4]+"\n" +
"<td>"+values[5]+"\n" +
"<td>"+values[6]+"\n" +
"</tr>\n" +
"</table>\n" +
"</center>\n" +
"  </fieldset>\n" +
"</center>\n" +
"</td>\n" +
"</html>";
    if(value==null||value.length()<1) 
        return;
    if(filename==null||filename.length()<1)
        return;
    //Encode
    value = Util.encodeUrl(value);
    
    //System.out.println( FileSystemStorage.getInstance().getAppHomePath());

    String fullPathToFile = FileSystemStorage.getInstance().getAppHomePath()+filename;
    Util.downloadUrlToFileSystemInBackground(URL+"?apikey="+APIKEY+"&value="+value, fullPathToFile);
    
    //FileSystemStorage fs = FileSystemStorage.getInstance();
    Display.getInstance().execute(fullPathToFile);



        
}

}