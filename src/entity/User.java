/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author user
 */
public class User {
    
     
    private String nom;
    private int id;
    private String login;
    private String date_naiss;
    private String numTel;
    private String password;
    private String email;
    private String username;

 
    public User(String nom, int id, String login, String date_naiss, String numTel, String password, String email, String username) {
        this.nom = nom;
        this.id = id;
        this.login = login;
        this.date_naiss = date_naiss;
        this.numTel = numTel;
        this.password = password;
        this.email = email;
        this.username = username;
    }

    public User(String nom, String login, String date_naiss, String numTel, String password, String mail, String username) {
        this.nom = nom;
        this.login = login;
        this.date_naiss = date_naiss;
        this.numTel = numTel;
        this.password = password;
        this.email = mail;
        this.username = username;
    }
    
    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }
    
       public String getNumTel() {
        return numTel;
    }

   

    public User(int id, String nom, String mail) {
        this.id = id;
        this.nom = nom;
    
        this.email = mail;
    }
    
    
    

    

    public User() {
    }

    public String getNom() {
        return nom;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getDate_naiss() {
        return date_naiss;
    }

    public String getNumTtel() {
        return numTel;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setDate_naiss(String date_naiss) {
        this.date_naiss = date_naiss;
    }

    public void setNumTtel(String numTtel) {
        this.numTel = numTtel;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String mail) {
        this.email = mail;
    }

    public void setUsername(String username) {
        this.username = username;
    }




    
    
}
