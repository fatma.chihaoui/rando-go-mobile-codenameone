/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.Image;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.mycompany.myapp.DetRandoForm;
import com.mycompany.myapp.MyApplication;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Me
 */
public class Randonnee {
    
    private int id;
    private String titre;
    private String Description;
    private int nb_place_min;
    private int nb_place_max;
    private Date date;
    private float nb_kilometres;
    private String etat;
    private float prix;
    private String type;
    private String destination;
    private String organisateur;
    private int isParticipating;
    private Date date_part;

    public Date getDate_part() {
        return date_part;
    }

    public void setDate_part(Date date_part) {
        this.date_part = date_part;
    }

    public int getIsParticipating() {
        return isParticipating;
    }

    public void setIsParticipating(int isParticipating) {
        this.isParticipating = isParticipating;
    }
    ArrayList<RandoImage> listImages ;
   


    public ArrayList<RandoImage> getListImages() {
        return listImages;
    }

    public void setListImages(ArrayList<RandoImage> listImages) {
        this.listImages = listImages;
    }
    
    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Randonnee{" + "id=" + id + ", titre=" + titre + ", Description=" + Description + ", nb_place_min=" + nb_place_min + ", nb_place_max=" + nb_place_max + ", date=" + date + ", nb_kilometres=" + nb_kilometres + ", etat=" + etat + ", prix=" + prix + ", type=" + type + ", destination=" + destination + ", listImages=" + listImages + '}';
    }



    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getNb_place_min() {
        return nb_place_min;
    }

    public void setNb_place_min(int nb_place_min) {
        this.nb_place_min = nb_place_min;
    }

    public int getNb_place_max() {
        return nb_place_max;
    }

    public void setNb_place_max(int nb_place_max) {
        this.nb_place_max = nb_place_max;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getNb_kilometres() {
        return nb_kilometres;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }

    public void setNb_kilometres(float nb_kilometres) {
        this.nb_kilometres = nb_kilometres;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
    
    
    public Container getContainer(){
        Button myBtn = new Button();
        myBtn.addActionListener(e -> {
            System.out.println("object clicked "+this.getTitre());
            (new DetRandoForm(MyApplication.theme, this)).showForm();
        });
        
        Calendar cal=Calendar.getInstance();
        cal.setTime(this.getDate());
        
        
        EncodedImage encodedImage=EncodedImage.createFromImage(MyApplication.theme.getImage("Loading.png"),false);
       
        Container mainContainer=new Container(new BoxLayout(BoxLayout.X_AXIS));
        mainContainer.setLeadComponent(myBtn);
        mainContainer.setUIID("mainContainer"+this.getId());
        mainContainer.getStyle().setBgColor(0xB3E5FC);
        mainContainer.getStyle().setBgTransparency(255);
        mainContainer.getStyle().setPadding(2,0,0,0);
        mainContainer.getStyle().setMargin(2,5,2,2);
        
        Container rightSubContainer =new Container(new BoxLayout(BoxLayout.Y_AXIS));
        rightSubContainer.add(this.getTitre());
        rightSubContainer.add(this.getDestination());
        rightSubContainer.add(this.getPrix()+"DT");
        rightSubContainer.add(cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR));
        
        rightSubContainer.getStyle().setFont(Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_SMALL));
        
       
        rightSubContainer.setUIID("rightSubContainer");

        
        
        Container leftSubContainer=new Container();
        leftSubContainer.setWidth(50);
        leftSubContainer.setHeight(50);
        Image i=URLImage.createToStorage(encodedImage,this.getListImages().get(0).getId()+"","http://localhost:8000/Uploads/"+this.getListImages().get(0).getPath(),URLImage.RESIZE_SCALE_TO_FILL);
        i=i.scaled(90, 100);   
        
        leftSubContainer.add(i);
        
        
        mainContainer.add(leftSubContainer);
        //mainContainer.add("image place");
        mainContainer.add(rightSubContainer);
        
        return mainContainer;
    }
    
    
    
}
